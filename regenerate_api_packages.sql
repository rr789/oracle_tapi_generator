/* 
--------------------------------------------------------
Drops all TAPIGen2 api's. They end in "_TPI". 
Then re-creates all table API's.  
--------------------------------------------------------
*/



/* 
--------------------------------------------------------
Drop all TAPIGen2 api's. They end in "_TPI". 
--------------------------------------------------------
*/
DECLARE cursor resultSet is   
    /* 
    Prepares statements like "drop TABLE TABLE_NAME CASCADE CONSTRAINTS".  
    Does not add semicolon to end of each statement since "execute immediate" has issues with it. 
    */
	SELECT 'drop '||object_type||' '|| object_name|| DECODE(OBJECT_TYPE,'TABLE',' CASCADE CONSTRAINTS','') as "FIELD1"  
	FROM user_objects 
	WHERE OBJECT_TYPE IN('PACKAGE') and object_name like ('%TPI'); /* Uses "TPI" instead of "API".  Some tables ending with "_GT" will get package ending in "_GTPI" so no underscore is put in the LIKE criteria. */

BEGIN 
    --Loop over resultset
    FOR rowObj IN resultSet LOOP
        Z_TAPI_GEN2__TEPLSQL.print_line_unlimited(rowObj.FIELD1); --Debugging.
        execute immediate rowObj.FIELD1;
    END LOOP;
END;
/



/* 
--------------------------------------------------------
Run TAPI2Gen to create packages for each table.  
Note: Each table must have primary key, or this will throw error.  
--------------------------------------------------------
*/
DECLARE cursor resultSet is   
    /* 
    Prepares statements like "drop TABLE ADDRESS_JOIN_BU_EXTERNAL_T CASCADE CONSTRAINTS".  
    Does not add semicolon to end since "execute immediate" has issues with it. 
    */
	SELECT   'erp.z_tapi_gen2.create_tapi_package (p_table_name => ''' || object_name || ''' , p_compile_table_api => TRUE)  ' as "FIELD1"  
	FROM user_objects 
	WHERE OBJECT_TYPE IN('TABLE')  and object_name NOT LIKE ('%Z_TAPI%'); /* TAPI2Gen has one table called Z_TAPI_TEMPLATES. */
	
BEGIN
    --Loop over resultset
    FOR rowObj IN resultSet LOOP
		Z_TAPI_GEN2__TEPLSQL.print_line_unlimited(rowObj.FIELD1); --Debugging.
		execute immediate 'BEGIN ' || rowObj.FIELD1 || '; END; ' ;
    END LOOP;
END;
/






