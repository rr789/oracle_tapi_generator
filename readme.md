Forked from https://github.com/osalvador/tapiGen2

Basically compiled all the scripts into one install file.  Updated package names to end in _tpi.  Couple other updates to keep char size <= 30.  

-------------------
INSTALL INSTRUCTIONS:
-------------------
1. Login as SYS user.  Run this statement.  
GRANT EXECUTE on  DBMS_CRYPTO to MY_USERNAME;

2. Disconnect from SYS. 

3. Login to the schema that has tables to generate API packages on.  

4. Run the install.sql script in SQL developer.   Ignore errors from three "DROP" rows in the "UN-INSTALL" section.    


-------------------
TO GENERATE API PACKAGE FOR SINGLE TABLE:
-------------------
exec z_tapi_gen2.create_tapi_package (p_table_name => 'TABLE_NAME', p_compile_table_api => TRUE);


-------------------
TO GENERATE API PACKAGES FOR ALL TABLES.  THIS WILL ALSO REMOVE/RE-CREATE ALL _TAPI PACKAGES.  
-------------------
Run regenerate_api_packages.sql

